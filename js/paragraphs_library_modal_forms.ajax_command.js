/**
 * @file
 * Handles AJAX submission and response in Views UI.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Ajax command to show certain buttons in the views edit form.
   *
   * @param {Drupal.Ajax} [ajax]
   *   An Ajax object.
   * @param {object} response
   *   The Ajax response.
   * @param {bool} response.changed
   *   Whether the state changed for the buttons or not.
   * @param {number} [status]
   *   The HTTP status code.
   */
  Drupal.AjaxCommands.prototype.paragraphs_library_modal_forms_input_value_updated = function (ajax, response, status) {

    if (!response.selector || !response.value) {
      return false;
    }

    var selector = $(response.selector);
    if (selector.is('input')) {
      selector.val(response.value);
    }
    else {
      selector.text(response.value);
    }

  };

}(jQuery, Drupal));
