<?php

namespace Drupal\paragraphs_library_modal_forms\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * The command to change the value of an element.
 */
class InputValueUpdateCommand implements CommandInterface {

  /**
   * A CSS selector string.
   *
   * @var string
   */
  protected $selector;

  /**
   * The data to pass on to the client side.
   *
   * @var string
   */
  protected $value;

  /**
   * Constructs a BaseCommand object.
   *
   * @param string $selector
   *   The css selector.
   * @param string $value
   *   The data to pass on to the client side.
   */
  public function __construct($selector, $value) {
    $this->selector = $selector;
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'paragraphs_library_modal_forms_input_value_updated',
      'selector' => $this->selector,
      'value' => $this->value,
    ];
  }

}
