<?php

namespace Drupal\paragraphs_library_modal_forms\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InsertCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Plugin\Field\FieldWidget\ParagraphsWidget;
use Drupal\paragraphs_library_modal_forms\Ajax\InputValueUpdateCommand;

/**
 * Returns responses for entity browser routes.
 */
class LabelController extends ControllerBase {

  /**
   * Callback on change paragraph library item.
   */
  public static function changeItemLabelAjax(array &$form, FormStateInterface $form_state) {

    $response = AjaxResponse::create();

    if (!$form_state->getErrors()) {
      $library_item_selector = \Drupal::request()->query->get('paragraphs_library_selector');

      $library_item = $form_state->getFormObject()->getEntity();

      $response->addCommand(new InputValueUpdateCommand('#' . $library_item_selector, $library_item->label()));
      $response->addCommand(new CloseModalDialogCommand());

      drupal_get_messages();
    }
    else {
      $message = [
        '#theme' => 'status_messages',
        '#message_list' => drupal_get_messages(),
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
      ];
      $messages = \Drupal::service('renderer')->render($message);
      $response->addCommand(new HtmlCommand('#' . Html::getClass($form['form_id']['#value']) . '-messages', $messages));
    }

    return $response;
  }

  /**
   * Callback on add or edit paragraph to library item.
   */
  public static function itemLabelAjaxModal(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();

    $submit = ParagraphsWidget::getSubmitElementInfo($form, $form_state, ParagraphsWidget::ACTION_POSITION_ACTIONS);

    $submit['element']['#prefix'] = '<div class="ajax-new-content">' . (isset($submit['element']['#prefix']) ? $submit['element']['#prefix'] : '');
    $submit['element']['#suffix'] = (isset($submit['element']['#suffix']) ? $submit['element']['#suffix'] : '') . '</div>';

    $html = \Drupal::service('renderer')->renderRoot($submit['element']);
    $response->setAttachments($submit['element']['#attached']);
    $response->addCommand(new InsertCommand(NULL, $html));

    $paragraph = $submit['widget_state']['paragraphs'][$submit['delta']]['entity'];
    if ($paragraph->hasField('field_reusable_paragraph')) {
      /** @var \Drupal\paragraphs_library\Entity\LibraryItem $library_item */
      $library_item = $paragraph->get('field_reusable_paragraph')->entity;
      if ($library_item) {
        if (isset($submit['element'][$submit['delta']]['subform']['field_reusable_paragraph']['widget'][0]['target_id']['#id'])) {
          $library_item_selector = $submit['element'][$submit['delta']]['subform']['field_reusable_paragraph']['widget'][0]['target_id']['#id'];
        }
        else {
          $library_item_selector = $submit['element'][$submit['delta']]['top']['summary']['#id'];
        }
        $library_item_form = \Drupal::service('entity.form_builder')->getForm($library_item, 'edit', ['redirect' => FALSE, 'paragraphs_library_selector' => $library_item_selector]);
        $library_item_form['#attached']['library'][] = 'core/drupal.dialog.ajax';
        $library_item_form['#attached']['library'][] = 'ajax_commands';

        $modal_title = t('Change label for library item?');
        $modal_content['form'] = $library_item_form;

        $response->addCommand(new OpenModalDialogCommand($modal_title, $modal_content, ['width' => 'auto', 'height' => 'auto']));
      }
    }

    return $response;
  }

}
